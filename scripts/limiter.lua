local _M = {}

local cache = ngx.shared.cache
local cjson = require("cjson")

--判断token是否正常
local function is_token_ok(token)
    if not token then
        ngx.exit(ngx.HTTP_FORBIDDEN)
    end
    local value = cache:get(token)
    if not value then
        return false
    end
    local obj = cjson.decode(value)
    --token不相等
    if obj["token_code"] ~= token then
        return false
    end
    --token非正常
    if obj["token_status"] ~= 0 then
        return false
    end
    --节点非正常
    if obj["endpoint_status"] ~= 0 then
        return false
    end  
    --用户非正常
    if obj["user_status"] ~= 0 then
        return false
    end   
    --用户未激活
    if obj["user_active"] ~= 1 then
        return false
    end    
    
    --非永久有效的token 并且 当前时间的时间戳大于截止过期时间的时间戳
    local token_type = obj["token_type"]
    local token_expire = tonumber(obj["token_expire"])
    local current_timestamp = ngx.now()
    if token_type ~= 1 and current_timestamp > token_expire then
        return false
    end
    return true
end

function _M.process()
    local token = ngx.req.get_uri_args()["token"]
    local ok = is_token_ok(token)
    if not ok then
        ngx.exit(ngx.HTTP_FORBIDDEN)
    end  
end


return _M
